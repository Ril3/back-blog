DROP TABLE IF EXISTS user;
CREATE TABLE user (  
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(255) NOT NULL
) default charset utf8 comment '';


DROP TABLE IF EXISTS post;
CREATE TABLE post(  
    user_id INT NOT NULL,
    CONSTRAINT FK_UserPost FOREIGN KEY(user_id) REFERENCES user(id),  
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    title VARCHAR (255),
    photo VARCHAR (255),
    body TEXT,
    date DATETIME
) default charset utf8 comment '';