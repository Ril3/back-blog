export class Post{
    user_id;
    id;
    title;
    photo;
    body;
    date;
    constructor(user_id, title, photo, body, date, id=null){
        this.user_id = user_id;
        this.title = title;
        this.photo = photo;
        this.body = body;
        this.date = date;
        this.id = id
    }
}