import fs from 'fs';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRepo } from '../repository/UserRepo'


const privateKey = fs.readFileSync('src/config/id_rsa');
const publicKey = fs.readFileSync('src/config/id_rsa.pub');

/**
 * Function that generates a JWT based on the private key.
 * It is currently configured to expire after 1 month (60*60*24*31 seconds) but this can be changed 
 * @param {object} payload le body qui sera mis dans le token
 * @returns le JWT
 */
export function generateToken(payload, expire = 60*60) {
    const token = jwt.sign(payload, privateKey,{algorithm: 'RS256', expiresIn: expire });
    return token;
}

/**
 * Fonction qui configure la stratégie JWT de passport, il va chercher le token dans les headers de la 
 * requête en mode {"authorization":"Bearer leToken"}
 */
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {
        //Si on arrive ici, c'est que le token est valide et a été décodé, on se
        //retrouve avec le body du token (payload) et on doit ce servir de celui ci
        //pour récupérer le User correspondant à ce token, on utilise ici la méthode
        //findByEmail pour récupérer le User et pouvoir y donner accès dans les routes protégées
        try {
            const user = await UserRepo.findEmail(payload.email);
            
            if(user) {
               
                return done(null, user);
            }
            
            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}
