import 'dotenv-flow/config';
import { server } from "./server";

const port = process.env.PORT || 7000;

server.listen(port, ()=>{
    console.log('listening  http://localhost:'+port)
})