import { Router } from "express";
import { UserRepo } from "../repository/UserRepo";
import bcrypt from "bcrypt";
import { generateToken } from "../config/token";
import passport from "passport";

export const userController = Router();

//Get all users
userController.get("/all", async(req, res)=>{
    const users = await UserRepo.all();
    res.json(users)
})



//Add user
userController.post('/add', async (req, res)=>{
    req.body.password = await bcrypt.hash(req.body.password, 12)
    await UserRepo.add(req.body);
    res.end();
  }) 


//Register
userController.post("/register", async (req, res) => {
    console.log(req.body);
    const exist = await UserRepo.findEmail(req.body.email);
    if(!exist){
        try {
            const hashedPwd = await bcrypt.hash(req.body.password, 12);
            const insertResult = await UserRepo.add({
              email: req.body.email,
              password: hashedPwd,
              role: "user"
            });
            res.send(insertResult);
          } catch (error) {
            console.log(error);
            res.status(500).send("Internal Server error Occured");
          }
    }else{
        res.status(400).json({error:"user allready exist"})
    }
    
  });

//Login
userController.post("/login", async (req, res) => {
    try {
      const user = await UserRepo.findEmail(req.body.email);
      console.log(user);
      if (user) {
        const cmp = await bcrypt.compare(req.body.password, user.password);
        console.log(cmp)
        if (cmp) {
          //   ..... further code to maintain authentication like jwt
         res.json({
            user,
            token: generateToken({
                email: user.email,
                id:user.id,
                role:user.role
            })
        });
        return;
        } 
      } else {
        res.send("Wrong username or password.");
      }
    } catch (error) {
      console.log(error);
      res.status(500).send("Internal Server error Occured");
    }
  });


//Delete user
userController.delete('/delete/:id',async (req,res)=>{
    try{
        await UserRepo.deleteUser(req.params.id);
        res.status(204).end()
    }catch (e){
        console.log(e)
        res.status(500).end()
    }
})

//Update User
userController.put('/update', async (req,resp)=>{
  await UserRepo.updateUser(req.body);
  resp.end();
})


userController.get('/account', passport.authenticate('jwt', {session:false}), (req,res) => {
  res.json(req.user);
});

//Get user by id
userController.get("/:id", async(req, res)=>{
  let user = await UserRepo.findId(req.params.id);
  if(!user){
      console.log(req.body)
      res.status(404).json ({error : 'User is not found!!!'});
      return;
  }
  res.json(user);
}) 