import { Router } from "express";
import PostReposetory from "../repository/PostRepo";
import passport from 'passport';

export const postController = Router();

postController.get('/all', async (req, res) => {
    try{
        const posts = await PostReposetory.findAll();
        res.json(posts)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


//Add post
postController.post("/add", passport.authenticate('jwt', {session:false}), async (req,res) => {
    console.log(req.body, req.user.id)
    let id = await PostReposetory.add(req.body, req.user.id);
    res.status(201).json(await PostReposetory.findId(id));
});

//Delete post
postController.delete('/delete/:id', passport.authenticate('jwt', {session:false}), async (req,res)=>{
    try{
        await PostReposetory.deletePost(req.params.id);
        res.status(204).json({"message":"Post deleted"}).end()
    }catch (e){
        console.log(e)
        res.status(500).end()
    }

})


//Update post
postController.put('/update', async (req,resp)=>{
    await PostReposetory.updatePost(req.body);
    resp.end();
  })


  