import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { postController } from './controller/post-controller';
import { configurePassport } from './config/token';
import  passport  from 'passport';


configurePassport();


export const server = express();
server.use(passport.initialize());



server.use(express.json());
server.use(cors())
server.use("/api/user", userController)
server.use("/api/post", postController)