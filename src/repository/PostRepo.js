import { Post } from "../entity/Post";
import { connection } from "./connection";

export default class PostReposetory{


    static async findAll() {
        const [rows] = await connection.query('SELECT * FROM post');
        return rows.map(row => new Post(row.userId, row.title, row.photo, row.body, row.date, row.id));
    }

    /**
     * Methode that add one post into the database
     * @param {Number} post 
     * @param {Number} userId 
     */
    static async add(post,userId) {
        const [row] = await connection.execute('INSERT INTO post (user_id, title, photo, body, date) VALUES (?,?,?,?,?)', [userId, post.title, post.photo, post.body, post.date]);
        return post.id= row.insertId;
    };

    /**
    * Methode that find one user by id
    * @param {Number} id of one post
    * @returns {Promise<User>} object of post
    */
     static async findId(id){
        const [rows] = await connection.execute('SELECT * FROM post WHERE id=?', [id]);
        if(rows.length === 1){
            return new Post(rows[0].user_id, rows[0].title, rows[0].photo, rows[0].body, rows[0].date, rows[0].id)
        }
        return null;
    }

    //delete
    static async deletePost(id){
        await connection.execute('DELETE FROM post WHERE id=?', [id]);
     }

    //Update
        /**
     * Methode that edit/update an post
     * @param {Object} post 
     */
         static async updatePost(post){
            await connection.query('UPDATE post SET title=?, photo=?, body=?, date=? WHERE id=?', [post.title, post.photo, post.body, post.date, post.id])
        }
}