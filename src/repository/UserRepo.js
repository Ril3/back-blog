import User from "../entity/User";
import { connection } from "./connection";

export class UserRepo{
    /**
    * Methode that find all users
    * @returns {Promise<User>} table of users
    */
    static async all(){
        const [rows] = await connection.query("SELECT * FROM user");
        const users = [];
        for(let row of rows){
            let instance = new User(row.email, row.password, row.role, row.id);
            users.push(instance)
        }
        return users
    }

    /**
    * Methode that find one user by id
    * @param {Number} id of a user
    * @returns {Promise<User>}
    */
     static async findId(id){
        const [rows] = await connection.execute('SELECT * FROM user WHERE id=?', [id]);
        if(rows.length === 1){
            return new User(rows[0].email, rows[0].password, rows[0].role, rows[0].id)
        }
        return null;
    }

    /**
     * Method that add one user into the database
     * @param {Object} user 
     */
    static async add(user) {
        const [rows] = await connection.execute('INSERT INTO user (email, password, role) VALUES (?,?,?)', [user.email, user.password, user.role]);
        user.id= rows.insertId;
    }

    /**
     * Methode that find user by email
     * @param {String} email 
     * @returns {Promise<User>}
     */
    static async findEmail(email){
        const [rows] = await connection.execute('SELECT * FROM user WHERE email=?', [email]);
        if(rows.length === 1){
            return new User(rows[0].email, rows[0].password, rows[0].role, rows[0].id)
        }
        return null; 
    }

    /**
     * Methode that delete an user
     * @param {Number} id 
    */
    static async deleteUser(id){
       await connection.execute('DELETE FROM user WHERE id=?', [id]);
    }

    /**
     * Methode that edit/update an user
     * @param {Object} user 
     */
    static async updateUser(user){
        await connection.query('UPDATE user SET email=?, password=?, role=? WHERE id=?', [user.email, user.password, user.role,user.id])
    }
}
